module yixiang.co/yshop

go 1.15

require github.com/beego/beego/v2 v2.0.1

require (
	github.com/casbin/casbin v1.9.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/mojocn/base64Captcha v1.3.1
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/image v0.0.0-20201208152932-35266b937fa6 // indirect
)
